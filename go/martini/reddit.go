package main

import (
  "github.com/go-martini/martini"
  //"html/template"
)

func hello() string {
  return "Hello World !"
}

func hello_you(params martini.Params) string {
  return "Hello " + params["name"] + " !"
}

func main() {
  m := martini.Classic()
  
  m.Get("/", hello)
  m.Get("/:name", hello_you)
  m.Run()
}
# Simple reddit clone in Go with Martini

## Dependencies

* Go
* http://github.com/go-martini/martini
* Go's html/template

## Installing those dependancies

    $ go get github.com/go-martini/martini
    $ go get html/template